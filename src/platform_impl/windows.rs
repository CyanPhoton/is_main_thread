#![cfg(target_os = "windows")]

use winapi::um::processthreadsapi;
use winapi::shared::minwindef::DWORD;

pub fn is_main_thread() -> Option<bool> {
    let thread_id = unsafe { processthreadsapi::GetCurrentThreadId() };

    Some(thread_id == main_thread_id())
}

fn main_thread_id() -> DWORD {
    static mut MAIN_THREAD_ID: DWORD = 0;
    #[used]
    #[allow(non_upper_case_globals)]
    #[link_section = ".CRT$XCU"]
    static INIT_MAIN_THREAD_ID: unsafe fn() = {
        unsafe fn initer() {
            MAIN_THREAD_ID = processthreadsapi::GetCurrentThreadId();
        }
        initer
    };

    unsafe { MAIN_THREAD_ID }
}