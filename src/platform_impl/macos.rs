#![cfg(target_os = "macos")]

pub fn is_main_thread() -> Option<bool> {
    Some(msg_send![class!(NSThread), isMainThread])
}